package dev.exercise.ponyvillebank.dto;

import dev.exercise.ponyvillebank.common.marker.IDto;
import dev.exercise.ponyvillebank.common.marker.IResponse;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class ResponseDisplayMovements implements Serializable, IDto, IResponse {

    private static final long serialVersionUID = 1087343275955229205L;

    private List<DisplayMovementDto> accountMovements = new ArrayList<>();

    public List<DisplayMovementDto> getAccountMovements() {
        return Collections.unmodifiableList(accountMovements);
    }

    public void addDisplayMovement(DisplayMovementDto displayMovementDto){
        if(Objects.nonNull(displayMovementDto)){
            accountMovements.add(displayMovementDto);
        }
    }

    public void addAll(List<DisplayMovementDto> displayMovementDtoList) {
        if(Objects.nonNull(displayMovementDtoList)) {
            this.accountMovements.addAll(displayMovementDtoList);
        }
    }
}
