package dev.exercise.ponyvillebank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PonyvillebankApplication {

	public static void main(String[] args) {
		SpringApplication.run(PonyvillebankApplication.class, args);
	}

}
