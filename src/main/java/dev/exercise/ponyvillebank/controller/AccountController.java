package dev.exercise.ponyvillebank.controller;

import dev.exercise.ponyvillebank.dto.*;
import dev.exercise.ponyvillebank.service.IOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@RestController
@RequestMapping("/api/account")
public class AccountController {

    @Autowired
    @Qualifier("accountMovementService")
    IOperation<AccountMovementDto, AccountMovementDto> accountMovementService;

    @Autowired
    @Qualifier("displayMovementsService")
    IOperation<RequestAccountMovements, ResponseDisplayMovements> displayMovementsService;

    @Autowired
    @Qualifier("createAccountService")
    IOperation<RequestCreateAccountDto, AccountDto> createAccountOperation;

    @PostMapping("/create")
    public ResponseEntity<AccountDto> createAccount(@RequestBody RequestCreateAccountDto requestCreateAccountDto){
        AccountDto responseDto = createAccountOperation.with(requestCreateAccountDto);
        return new ResponseEntity<>(responseDto, HttpStatus.CREATED);
    }

    @PostMapping("/{accountId}/deposit/{amount}")
    public ResponseEntity<RequestDepositDto> deposit(@PathVariable("accountId") Long accountId,
                                                     @PathVariable("amount") BigDecimal amount){
        RequestDepositDto requestDepositDto =new RequestDepositDto();
        requestDepositDto.setAccountId(accountId);
        requestDepositDto.setAmount(amount);

        AccountMovementDto depositMovement = AccountMovementDto.depositMovement(requestDepositDto);
        accountMovementService.with(depositMovement);
        return new ResponseEntity<>(requestDepositDto, HttpStatus.CREATED);
    }

    @PostMapping("/{accountId}/withdraw/{amount}")
    public ResponseEntity<RequestWithdrawDto> withdraw(@PathVariable("accountId") Long accountId,
                                                       @PathVariable("amount") BigDecimal amount){

        RequestWithdrawDto requestWithdrawDto =new RequestWithdrawDto();
        requestWithdrawDto.setAccountId(accountId);
        requestWithdrawDto.setAmount(amount);
        AccountMovementDto withdrawMovement = AccountMovementDto.withdrawMovement(requestWithdrawDto);
        accountMovementService.with(withdrawMovement);
        return new ResponseEntity<>(requestWithdrawDto, HttpStatus.CREATED);
    }

    @GetMapping("/{accountId}/movements")
    public ResponseEntity<ResponseDisplayMovements> showMyMoney(@PathVariable("accountId") Long accountId){

        RequestAccountMovements request = RequestAccountMovements.of(accountId);
        ResponseDisplayMovements response = displayMovementsService.with(request);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


}
