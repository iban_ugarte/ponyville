package dev.exercise.ponyvillebank.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class PonyvilleController {

    @GetMapping("/welcome")
    public ResponseEntity<String> welcome(){
        StringBuilder sb = new StringBuilder("Welcome to Ponyville Bank !!!")
                .append("\n You can operate with your account once you've create it:")
                .append("\n\t - [POST] Create: /api/account/create")
                .append("\n\n After creating your account you can:")
                .append("\n\t - [POST] Deposit:   /api/account/{accountId}/deposit/{amount}")
                .append("\n\t - [POST] Withdraw:  /api/account/{accountId}/withdraw/{amount}")
                .append("\n\t - [GET]  Movements: /api/account/{accountId}/movements")

                .append("\n\n ENJOY PONYVILLE BANK !!");

        return new ResponseEntity<>(sb.toString(), HttpStatus.OK);
    }


}
