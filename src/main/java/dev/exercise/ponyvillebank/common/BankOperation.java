package dev.exercise.ponyvillebank.common;

public enum BankOperation {
	CREATE_ACCOUNT,
	DEPOSIT,
	WITHDRAW;
}
