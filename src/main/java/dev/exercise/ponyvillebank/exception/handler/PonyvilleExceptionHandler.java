package dev.exercise.ponyvillebank.exception.handler;

import dev.exercise.ponyvillebank.exception.AccountNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@ControllerAdvice
public class PonyvilleExceptionHandler extends ResponseEntityExceptionHandler {

    private ResponseEntity<PonyvilleErrorResponse> buildResponse(HttpServletRequest httpServletRequest,
                                                                 RuntimeException ex,
                                                                 HttpStatus httpStatus){
        PonyvilleErrorResponse errorResponse = new PonyvilleErrorResponse();
        errorResponse.setDate(LocalDateTime.now());
        errorResponse.setErrorMessage(ex.getMessage());
        errorResponse.setStatus(httpStatus.value());
        errorResponse.setPath(httpServletRequest.getServletPath());
        return new ResponseEntity<>(errorResponse, httpStatus);
    }

    @ExceptionHandler(AccountNotFoundException.class)
    public ResponseEntity<PonyvilleErrorResponse> handleAccountNotFoundException(
            AccountNotFoundException ex, final HttpServletRequest httpServletRequest) {

        return buildResponse(httpServletRequest, ex, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(IllegalStateException.class)
    public ResponseEntity<PonyvilleErrorResponse> handleIllegalStateException(
            IllegalStateException ex, final HttpServletRequest httpServletRequest) {

        return buildResponse(httpServletRequest, ex, HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<PonyvilleErrorResponse> handleIllegalArgumentException(
            IllegalArgumentException ex, final HttpServletRequest httpServletRequest) {

        return buildResponse(httpServletRequest, ex, HttpStatus.NOT_ACCEPTABLE);

    }

    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<PonyvilleErrorResponse> handleNullPointerException(
            NullPointerException ex, final HttpServletRequest httpServletRequest) {

        return buildResponse(httpServletRequest, ex, HttpStatus.BAD_REQUEST);

    }
}
