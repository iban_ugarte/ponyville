package dev.exercise.ponyvillebank.repository;

import dev.exercise.ponyvillebank.model.Account;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepo extends CrudRepository<Account, Long> {
}
