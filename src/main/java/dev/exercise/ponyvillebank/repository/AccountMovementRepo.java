package dev.exercise.ponyvillebank.repository;

import dev.exercise.ponyvillebank.model.AccountMovement;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;

public interface AccountMovementRepo extends CrudRepository<AccountMovement, Long> {

    @Query("SELECT am FROM AccountMovement am WHERE am.account.accountId = :accountId")
    Collection<AccountMovement> findAllAccountMovementsByAccountId(@Param("accountId") Long accountId);
}
