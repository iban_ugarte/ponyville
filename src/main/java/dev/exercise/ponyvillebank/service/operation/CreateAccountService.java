package dev.exercise.ponyvillebank.service.operation;

import dev.exercise.ponyvillebank.common.BankOperation;
import dev.exercise.ponyvillebank.dto.AccountDto;
import dev.exercise.ponyvillebank.dto.RequestCreateAccountDto;
import dev.exercise.ponyvillebank.model.Account;
import dev.exercise.ponyvillebank.model.AccountMovement;
import dev.exercise.ponyvillebank.parser.IParser;
import dev.exercise.ponyvillebank.repository.AccountRepo;
import dev.exercise.ponyvillebank.service.IOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;


@Service("createAccountService")
public class CreateAccountService implements IOperation<RequestCreateAccountDto, AccountDto> {

    private static final long serialVersionUID = 3444763930116234017L;

    @Autowired
    private AccountRepo repo;

    @Autowired
    @Qualifier("accountParser")
    private IParser<Account, AccountDto> accountParser;

    @Override
    public AccountDto with(RequestCreateAccountDto request) {
        Account account = getAccountFrom(request);
        Account createdAccount =  repo.save(account);
        return accountParser.parse(createdAccount);
    }

    private Account getAccountFrom(RequestCreateAccountDto request) {
        Account account = new Account();
        account.setBankAccountHolder(request.getBancAccountHolder());
        BigDecimal amount = BigDecimal.ZERO;
        if(Objects.nonNull(request.getInitAmount())){
            amount = request.getInitAmount();
        }
        account.setBalance(amount);

        AccountMovement movement = new AccountMovement();
        movement.setOperation(BankOperation.CREATE_ACCOUNT);
        movement.setDate(LocalDate.now());
        movement.setBalance(amount);
        movement.setAmount(amount);
        account.addAccountMovement(movement);
        return account;
    }
}
