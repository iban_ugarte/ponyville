package dev.exercise.ponyvillebank.service.operation;

import dev.exercise.ponyvillebank.dto.AccountMovementDto;
import dev.exercise.ponyvillebank.exception.AccountNotFoundException;
import dev.exercise.ponyvillebank.model.Account;
import dev.exercise.ponyvillebank.model.AccountMovement;
import dev.exercise.ponyvillebank.repository.AccountMovementRepo;
import dev.exercise.ponyvillebank.repository.AccountRepo;
import dev.exercise.ponyvillebank.service.IOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

@Service("accountMovementService")
@Transactional
public class AccountMovementService implements IOperation<AccountMovementDto, AccountMovementDto> {

    private static final long serialVersionUID = 2794523704173778961L;

    @Autowired
    private AccountMovementRepo accountMovementRepo;

    @Autowired
    private AccountRepo accountRepo;

    @Override
    public AccountMovementDto with(AccountMovementDto data) {

        Objects.requireNonNull(data, "If you want to move your account, send data ;)");

        Account account = getAccount(data);
        doAccountMovement(data, account);
        updateAccountBalance(data, account);

        return data;
    }

    private void updateAccountBalance(AccountMovementDto data, Account account) {
        account.setBalance(this.calculateAccountBalance(data, account));
        accountRepo.save(account);
    }

    private BigDecimal calculateAccountBalance(AccountMovementDto data, Account account) {
        return account.getBalance().add(data.getAmount());
    }

    private void doAccountMovement(AccountMovementDto data, Account account) {
        AccountMovement movement = new AccountMovement();
        movement.setAccount(account);
        movement.setAmount(data.getAmount());
        movement.setOperation(data.getBankOperation());
        movement.setDate(LocalDate.now());
        movement.setBalance(this.calculateAccountBalance(data, account));
        accountMovementRepo.save(movement);
    }

    private Account getAccount(AccountMovementDto data) {

        Account account = new Account();
        account.setAccountId(data.getAccountId());
        return accountRepo.findById(data.getAccountId())
                .orElseThrow(() -> AccountNotFoundException.of(data.getAccountId()));
    }

}
