package dev.exercise.ponyvillebank.service.operation;

import dev.exercise.ponyvillebank.dto.DisplayMovementDto;
import dev.exercise.ponyvillebank.dto.RequestAccountMovements;
import dev.exercise.ponyvillebank.dto.ResponseDisplayMovements;
import dev.exercise.ponyvillebank.model.AccountMovement;
import dev.exercise.ponyvillebank.parser.IParser;
import dev.exercise.ponyvillebank.repository.AccountMovementRepo;
import dev.exercise.ponyvillebank.service.IOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service("displayMovementsService")
public class DisplayMovementsService implements IOperation<RequestAccountMovements, ResponseDisplayMovements> {

    private static final long serialVersionUID = 7747041889631733350L;

    @Autowired
    private AccountMovementRepo repo;

    @Autowired
    @Qualifier("displayMovementParser")
    private IParser<AccountMovement, DisplayMovementDto> parser;

    @Override
    public ResponseDisplayMovements with(RequestAccountMovements data) {
        Collection<AccountMovement> movements = repo.findAllAccountMovementsByAccountId(data.getAccountId());
        ResponseDisplayMovements response = new ResponseDisplayMovements();
        response.addAll(parser.toDtoList(new ArrayList<>(movements)));

        return response;
    }
}
