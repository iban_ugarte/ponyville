package dev.exercise.ponyvillebank.service;

import dev.exercise.ponyvillebank.common.marker.IRequest;
import dev.exercise.ponyvillebank.common.marker.IResponse;

import java.io.Serializable;

public interface IOperation<T extends IRequest, R extends IResponse> extends Serializable {
	R with(T data);
}
