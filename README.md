# PONYVILLE BANK



## INSTALLATION & EXECUTION

With maven installed and from the terminal/console, move to de project directory and type: 

- For installing maven in a Mac: https://mkyong.com/maven/install-maven-on-mac-osx/
- For installing maven in Windows: https://mkyong.com/maven/how-to-install-maven-in-windows/

```bash
$ mvn spring-boot:run
```

For testing (I've used postman), install:

- Postman (https://www.postman.com/) or 
- SOAPUI (https://www.soapui.org/downloads/soapui/)



## Operations

The context path is: http://localhost:8080/ponyvillebank/api

| ENDPOINT                                    | DESCRIPTION                              |
| ------------------------------------------- | ---------------------------------------- |
| /welcome                                    | Being polite is a must !                 |
| /account/create                             | Create an account in Ponyville bank      |
| /account/\{accountId\}/deposit/\{amount\}   | Deposit money                            |
| /account//\{accountId\}/withdraw/\{amount\} | Withdraw money                           |
| /account/\{accountId\}/movements            | Display movements from the given account |



### 1. Being polite is a must

http://localhost:8080/ponyvillebank/api/welcome

### 2. Open an account to operate

http://localhost:8080/ponyvillebank/api/account/create

Json body:

```json
{
	"bancAccountHolder" : "Comalatech",
	"initAmount": 500.00
}
```

### 3. Deposit money

http://localhost:8080/ponyvillebank/api/account/1/deposit/200

### 4. Withdraw money

http://localhost:8080/ponyvillebank/api/account/1/withdraw/300

### 5. Show me the money

http://localhost:8080/ponyvillebank/api/account/1/movements



## Database

It's a h2 in memory database. 

You can access the console by running the application and typing in your browser: 
```
http://localhost:8080/ponyvillebank/h2-console
```
This is the jdbc connection:
```
jdbc:h2:mem:ponyvillebank
```
